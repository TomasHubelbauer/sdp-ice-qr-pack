# SDP ICE pack

This library is used to pack and unpack SDP and ICE WebRTC messages for QR code alphabets (numeric, alphanumeric and byte).

It will work standalone, but I develop it for my [QR channel](https://gitlab.com/TomasHubelbauer/qr-channel) library.

## Contributing

To-do:

- [ ] Use external library for reading and writing SDP and ICE `InitDict`s
- [ ] Implement the logic for picking out the important parts (ICE user name and password, certificate hash, ICE candidate info, …)
- [ ] Implement a way to specify the desired QR alphabet
